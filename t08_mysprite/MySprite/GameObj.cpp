#include "GameObj.h"

GameObj::GameObj()
{
	//constructor
}

GameObj::~GameObj()
{
	//deconstructor
}
//sets initial values used in gameobject
void GameObj::Initialise(MYVEC2 position, float rot, std::string tex, MYVEC2 centre, MYRECT textureArea, MYVEC2 objscale)
{
	origin = centre;
	pos = position;
	rotation = rot;
	texName = tex;		
	texArea = textureArea;	
	scale = objscale;
}
//get functions
MYVEC2 GameObj::GetPos()
{
	return pos;
}
float GameObj::GetRot()
{
	return rotation;
}
std::string GameObj::GetTexName()
{
	return texName;
}
MYVEC2 GameObj::GetOrigin()
{
	return origin;
}
MYRECT GameObj::GetTexArea()
{
	return texArea;
}
MYVEC2 GameObj::GetScale()
{
	return scale;
}
//set functions
void GameObj::SetRotation(float newRot)
{
	rotation = newRot;
}