#ifndef GAMEOBJ_H
#define GAMEOBJ_H

#include <string>


//didnt want to include more modules/libraries to this so i just created my own version of them for better data handling
struct MYRECT {
public:
	float left, top, right, bottom;
	operator MYRECT() {
		return MYRECT{ left,top,right,bottom };
	}
};

struct MYVEC2 {
public:
	float x, y;
	operator MYVEC2() {
		return MYVEC2{ x,y };
	}
};


class GameObj {
public:
	GameObj();	
	~GameObj();
	void Initialise(MYVEC2, float rot, std::string tex, MYVEC2, MYRECT, MYVEC2);
	//getter and setter functions
	MYVEC2 GetPos();
	float GetRot();
	std::string GetTexName();
	MYVEC2 GetOrigin();
	MYRECT GetTexArea();
	MYVEC2 GetScale();
	void SetRotation(float radians);
private:
	MYVEC2 pos;
	float rotation;
	std::string texName;
	MYVEC2 origin;
	MYRECT texArea;
	MYVEC2 scale;
};

#endif // !GAMEOBJ_H