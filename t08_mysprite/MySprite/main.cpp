#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>
#include <ctime>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "CommonStates.h"
#include "SimpleMath.h"
#include "SpriteBatch.h"
#include "TexCache.h"
#include "Sprite.h"
#include "GameObj.h"
#include "SpriteFont.h"
#include "Singleton.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


/*
*/
class Game: public Singleton<Game>
{
public:
	enum class State { PLAY, ENDGAME_CORRECT, ENDGAME_INCORRECT };
	State state = State::PLAY;
	
	Game(MyD3D& d3d);
	vector<GameObj> Objects[10];

	void Interactions(int input);
	void Release();
	void Update(float dTime);
	void Render(float dTime);
	void Resize(int newWidth, int newHeight);
	void GenerateRandomClocks();

private:
	MyD3D& mD3D;
	SpriteBatch *pSB = nullptr;
	SpriteFont *gpFont = nullptr;
	string msg1,msg2;

	int OGSCREENX = 1280;
	int OGSCREENY = 720;

	float scrnScaleX;
	float scrnScaleY;

	int offset[3] = { 0, 0, 0 };
	int correct;
};

Game::Game(MyD3D& d3d)
	: mD3D(d3d), pSB(nullptr)
{
	pSB = new SpriteBatch(&mD3D.GetDeviceCtx());

	TexCache& cache = mD3D.GetCache();
	cache.LoadTexture(&d3d.GetDevice(), "clock_face.dds");
	cache.LoadTexture(&d3d.GetDevice(), "digital_clock.dds");
	cache.LoadTexture(&d3d.GetDevice(), "clock_hands.dds");

	scrnScaleX = 1;
	scrnScaleY = 1;

	gpFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/fonts/comicSansMS.spritefont");
	assert(gpFont);
	for (int i = 0; i < 10; i++)
	{
		Objects->push_back(GameObj());
		if (i < 1)
		{
			Objects->at(i).Initialise(MYVEC2{ WinUtil::Get().GetClientWidth() / 2.f, 2.f * WinUtil::Get().GetClientHeight() / 5.f }, 0.f, "digital_clock", MYVEC2{ 256.f, 256.f }, MYRECT{ 0.f, 0.f, 512.f, 350.f }, MYVEC2{1.f,1.f });
		}
		else if (i < 4)
		{
			Objects->at(i).Initialise(MYVEC2{ i * WinUtil::Get().GetClientWidth() / 4.f, 4.f * WinUtil::Get().GetClientHeight() / 5.f }, 0.f, "clock_face", MYVEC2{ 128.f, 128.f }, MYRECT{ 0.f, 0.f, 256.f, 256.f }, MYVEC2{ 1.f,1.f });
		}
		else if (i < 7)
		{
			Objects->at(i).Initialise(MYVEC2{ (i - 3) * WinUtil::Get().GetClientWidth() / 4.f, 4.f * WinUtil::Get().GetClientHeight() / 5.f }, 0.f, "clock_hands", MYVEC2{ 40.f, 317.f }, MYRECT{ 8.f, 137.f, 84.f, 480.f }, MYVEC2{ 0.2f, 0.2f }); //big hand
		}
		else
		{
			Objects->at(i).Initialise(MYVEC2{ (i - 6) * WinUtil::Get().GetClientWidth() / 4.f, 4.f * WinUtil::Get().GetClientHeight() / 5.f }, 0.f, "clock_hands", MYVEC2{ 26.f, 449.f }, MYRECT{ 98.f, 5.f, 148.f , 481.f }, MYVEC2{ .2f, .2f });	//small hand
		}
	}	
	srand(time(NULL));
	GenerateRandomClocks();
}


//any memory or resources we made need releasing at the end
void Game::Release()
{

	delete gpFont;
	gpFont = nullptr;

	delete pSB;
	pSB = nullptr;

}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{

	Resize(WinUtil::Get().GetClientWidth(), WinUtil::Get().GetClientHeight());
	if (state == State::PLAY)
	{
	
		time_t current = time(0);
		tm *t = localtime(&current);
		//updating the clocks to the current time + their offset
		for (int i = 0; i < 3; i++)
		{
			float hi = PI * (t->tm_min / 59.f) / 180.f;

			Objects->at(4 + i).SetRotation(((offset[i] / 60) + (float)(t->tm_hour % 12)) / 12 * 2 * PI);
			Objects->at(7 + i).SetRotation(((((offset[i] % 60) + t->tm_min) / 59.f)) * 2 * PI);
		}

		msg1 = "Which clock is showing the correct time ? 1 : left, 2 : centre, 3 : right";
		msg2 = to_string(t->tm_hour) + " : " + to_string(t->tm_min);
	}
	else if (state == State::ENDGAME_CORRECT)
	{
		msg1 = "You're incorrect! Press enter to try again";
		msg2 = "NICE ONE!";
	}
	else if (state == State::ENDGAME_INCORRECT)
	{
		msg1 = "You're correct! Press enter to try again";
		msg2 = "HA! HA! HA!";
	}


}

//called over and over, use it to render things
void Game::Render(float dTime)
{

	mD3D.BeginRender(Colours::Black);
	int h = WinUtil::Get().GetClientWidth();
	int w = WinUtil::Get().GetClientHeight();
	CommonStates dxstate(&mD3D.GetDevice());
	pSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(),&mD3D.GetWrapSampler());

	
	for (int i = 0; i < 10; i++)
	{
		Sprite spr(mD3D);
		spr.SetTex(*mD3D.GetCache().Get( Objects->at(i).GetTexName() ).pTex);
		spr.SetTexRect(RECTF{ Objects->at(i).GetTexArea().left, Objects->at(i).GetTexArea().top, Objects->at(i).GetTexArea().right, Objects->at(i).GetTexArea().bottom });
		spr.SetOrigin(Vector2(Objects->at(i).GetOrigin().x, Objects->at(i).GetOrigin().y));
		spr.SetScale(Vector2(Objects->at(i).GetScale().x * scrnScaleX , Objects->at(i).GetScale().y * scrnScaleY));
		spr.SetPos(Vector2(Objects->at(i).GetPos().x * scrnScaleX, Objects->at(i).GetPos().y * scrnScaleY));
		spr.SetRot(Objects->at(i).GetRot());

		spr.Draw(*pSB);
	}



	RECT dim1 = gpFont->MeasureDrawBounds(msg1.c_str(), Vector2(0, 0));
	Vector2 pos = Vector2{ (WinUtil::Get().GetClientWidth() / 2.f), (WinUtil::Get().GetClientHeight() / 2.f) + (dim1.bottom - dim1.top)*1.2f };

	gpFont->DrawString(pSB, msg1.c_str(), pos, Colours::White, 0, Vector2((float)dim1.right / 2.f, 0), Vector2(scrnScaleX, scrnScaleY));



	RECT dim2 = gpFont->MeasureDrawBounds(msg2.c_str(), Vector2(0, 0));
	Vector2 pos2 = Vector2{ WinUtil::Get().GetClientWidth() / 2.f , 1.25f * WinUtil::Get().GetClientHeight() / 5.f};// + (dim2.bottom - dim2.top)*1.2f 

	gpFont->DrawString(pSB, msg2.c_str(), pos2, Colours::White, 0, Vector2((float)dim2.right / 2.f, 0), Vector2(3 * scrnScaleX,3 * scrnScaleY));
	


	pSB->End();

	mD3D.EndRender();
}

void Game::GenerateRandomClocks()
{
	correct = rand() % 3;	
	
	for (int i = 0; i < 3; i++)
	{
		offset[i] = 0;
		if (correct != i)
		{
			offset[i] = rand() % 720;
		}
	}
	

	state = State::PLAY;
}

void Game::Interactions(int input)
{
	if (state == State::PLAY)
	{
		if ((input - 1) == correct)
		{
			state = State::ENDGAME_CORRECT;
		}
		else
		{
			state = State::ENDGAME_INCORRECT;
		}
	}
	else if(input == 4)
	{
		GenerateRandomClocks();
	}
}
void Game::Resize(int newWidth, int newHeight)
{
	scrnScaleX = (float)newWidth / (float)OGSCREENX;
	scrnScaleY = (float)newHeight / (float)OGSCREENY;
}


//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 13:
			Game::Get().Interactions(4);
			break;
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		case '1':
			Game::Get().Interactions(1);
			break;
		case '2':
			Game::Get().Interactions(2);
			break;
		case '3':
			Game::Get().Interactions(3);
			break;
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	
	int w(1280), h(720);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Clocks", MainWndProc, true))
		assert(false);

	MyD3D d3d;	
	if (!d3d.InitDirect3D(OnResize))
		assert(false);

	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("data/");
	Game game(d3d);
	
	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			game.Update(dTime);
			game.Render(dTime);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	game.Release();
	d3d.ReleaseD3D(true);	
	return 0;
}

